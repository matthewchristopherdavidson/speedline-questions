# Developer Written Screening Questions - Speedline

completed by *Matthew Davidson*

Some tests and benchmarks are included to prove correctness of results.

To run:
```
npm install
npm start
```

Note that the methods as given were slightly modified to return a result set rather than simply logging the output.

## 1) Describe some data sets that you would use to test any proposed implementation.

Some obvious missing cases in the given fixture are

+ Primitives and empty values, e.g. `1, false, 'not found', null, undefined`
+ Records with missing fields, e.g. `{"email":""}`
+ Records with duplicate toppings but in different order.

e.g. `['Pineapple','Salami']` and `['Salami','Pineapple']`

## 2) Do you see any bugs in the proposed implementations?

+ Both methods should filter out bad data types.
+ Both implementations naively assume that the toppings array is sorted alphabetically.  This means the solutions will not catch cases where the same toppings appear in a different order.

`printWinners1` could be improved as follows:
+ It should filter out disqualified entries to reduce the number of iterations.

```javascript
inputArray.sort((a, b) => {
    // Convert each toppings array to a string and do a string comparison
    return a.toppings.toString().localeCompare(b.toppings.toString());
});
```

will break if

+ `a` or `b` are primitives
+ `a.topping` or `b.topping` are null or undefined


## 3) Once any of these small bugs are fixed, will either of these approaches give us the output we're looking for?

Refer to `index.js` and `test/index.js`

Both `printWinners2Modified` and `printWinners1Modified` produce the desired output.

## 4) What are some advantages and disadvantages of these approaches?

The main scenario for choosing a solution like `printWinners1` over `printWinners2`
would be when working with very large data sets, where in-memory data-structures can grow large.  The reason I don't think that matters for this scenario is that if an input array is large enough to create that kind of backpressure, there is already a problem and the data set should probably be chunked into smaller batches.

`printWinners2` is faster than `printWinners1`.
`printWinners1Modified` is faster than `printWinners1`.
`printWinners2Modified` is faster than `printWinners1Modified`.

It's worth noting that using a `Map` is noticeably quicker than using an `Object`
in part because it implements `forEach` using a generator.

## 5) What approach would you suggest if you were querying a database instead of working with an in-memory structure?

Assuming `DESCRIBE orders;` returns something like

```
| Field   |   Type        | Null  | Key | Default         | Extra |
| id	    |  int(11)      |	NO	  | PRI	| auto_increment  |       |
| email   |	varchar(45)   |	YES   |     |               	|       |		
|toppings |	varchar(255)  |	YES	  |     |		              |       |
```

And toppings are stored as `topping1,topping2...`

Then

```sql
SELECT orders.email
FROM orders,
(
  # subquery to get the topping stats
  SELECT
  toppings,
  COUNT(*) as occurrences,
  # hack to measure list size :)
  ROUND(LENGTH(toppings) - LENGTH(REPLACE(toppings,',',''))+1) AS topcount
  FROM orders
  WHERE email !='' AND email IS NOT NULL
  GROUP BY toppings
) as topstats
WHERE topstats.occurrences=1
AND orders.toppings = topstats.toppings
AND topstats.topcount>2
```
would return the correct result set.  

It would require the toppings to be consistently sorted.

One approach would be to use the `SET()` data type which would guarantee list element order,
e.g. for `SET('a','b','c','d','e','f')`,
inserting `'b,a'` might be stored with a binary representation such as `110000`
and you would always get back `'a,b'`

But this would have the overhead of modifying the DB schema when new options are added.

Another, better approach would be to enforce sort order at the ORM layer.

```php
// e.g. pre-save
foreach ($items as $item) {
  $item->setToppings(array_sort($item->getToppings());)
}
$items->save();
```

which would be easier to maintain.
