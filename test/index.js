let methods = require('..');
let assert = require('assert');
let orders = require('../orders.json');

let fixtures = {
    original: {
        data: orders.slice(),
        expected: 'email2@example.com'
    },
    bad: {
        data: orders.slice().concat([
            null,
            undefined,
            3,
            false,
            true,
            {
                email: ''
            }
        ]),
        expected: 'email2@example.com'
    },
    unsorted: {
        data: orders.slice().concat([{
                email: 'email6@example.com',
                toppings: ['Cheddar', 'Oregano', 'Garlic']
            },
            {
                email: 'email6@example.com',
                toppings: ['Deli Meats', 'Oregano', 'Pesto']
            },
        ]),
        expected: 'email6@example.com'
    }
};


describe('methods', () => {
    describe('printWinners1', () => {
        it('produces the correct result', () => {
            assert(methods.printWinners1(fixtures.original.data.slice()) === fixtures.original.expected);
        });
        it('does not handle garbage data', () => {
            assert.throws(function () {
                methods.printWinners1(fixtures.bad.data.slice());
            }, Error);
        });
        it('does not weed out unsorted duplicate orders', () => {
            assert(methods.printWinners1(fixtures.unsorted.data.slice()) !== fixtures.unsorted.expected);
        });
    });
    describe('printWinners2', () => {
        it('produces the correct result', () => {
            assert(methods.printWinners2(fixtures.original.data.slice()) === fixtures.original.expected);
        });
        it('does not handle garbage data safely', () => {
            assert.throws(function () {
                methods.printWinners2(fixtures.bad.data.slice());
            }, Error);
        });
        it('does not weed out unsorted duplicate orders', () => {
            assert(methods.printWinners2(fixtures.unsorted.data.slice()) !== fixtures.unsorted.expected);
        });
    });
    describe('printWinners1Modified', () => {
        it('produces the correct result', () => {
            assert(methods.printWinners1Modified(fixtures.original.data.slice()) === fixtures.original.expected);
        });
        it('handles garbage data safely', () => {
            assert.doesNotThrow(function () {
                methods.printWinners1Modified(fixtures.bad.data.slice());
            }, Error);
        });
        it('weeds out unsorted duplicate orders', () => {
            assert(methods.printWinners1Modified(fixtures.unsorted.data.slice()) === fixtures.unsorted.expected);
        });
    });
    describe('printWinners2Modified', () => {
        it('produces the correct result', () => {
            assert(methods.printWinners2Modified(fixtures.original.data.slice()) === fixtures.original.expected);
        });
        it('handles garbage data safely', () => {
            assert.doesNotThrow(function () {
                methods.printWinners2Modified(fixtures.bad.data.slice());
            }, Error);
        });
        it('weeds out unsorted duplicate orders', () => {
            assert(methods.printWinners2Modified(fixtures.unsorted.data.slice()) === fixtures.unsorted.expected);
        });
    });
});
