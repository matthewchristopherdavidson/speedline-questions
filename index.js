const assert = require('assert');

/**
 * only log if debug flag is set.
 * @param  {Boolean} debug
 * @return Function
 */
function logMethod(debug) {
    return debug ? function () {
        console.log(...arguments);
    } : function () {};
}

/**
 * if the required fields are not present, return false;
 * @param  {Object} item
 * @return Boolean
 */
function isValidOrder(item) {
    return item &&
        item.email &&
        item.toppings &&
        item.toppings.length === 3 &&
        item.toppings.toString;
}

/**
 * if the required fields are not present, return false;
 * otherwise, sort the item's toppings and return.
 * @param  {Object} item
 * @return Mixed
 */
function validateAndSort(item) {
    return isValidOrder(item) &&
        item.toppings.sort((a, b) => a.localeCompare(b));
}


function printWinners1(inputArray, debug) {
    let log = logMethod(debug);
    let results = [];
    let previousEmail = '';
    let previousToppingsAsString = '';
    let previousToppingCount = 0;
    let numberOfSimilarOrders = 0;

    // Perform a QuickSort on the array.  We provide an anonymous function to do the comparisons.
    inputArray.sort((a, b) => {
        // Convert each toppings array to a string and do a string comparison
        return a.toppings.toString().localeCompare(b.toppings.toString());
    });
    // Iterate through the array, with "order" being each item in the array.
    inputArray.map((order) => {
        let toppingsAsString = order.toppings.toString();
        if (toppingsAsString === previousToppingsAsString) {
            numberOfSimilarOrders++;
        } else {
            if ((numberOfSimilarOrders === 1) && (previousToppingCount === 3) && (previousEmail) !== '') {
                results.push(previousEmail);
                log(previousEmail);
            }
            previousToppingsAsString = toppingsAsString;
            previousEmail = order.email;
            previousToppingCount = order.toppings.length;
            numberOfSimilarOrders = 1;
        }
    });
    return results.toString();
}
exports.printWinners1 = printWinners1;

function printWinners1Modified(inputArray, debug) {
    assert(Array.isArray(inputArray));
    let log = logMethod(debug);
    let results = [];
    let previousEmail = '';
    let previousToppingsAsString = '';
    let previousToppingCount = 0;
    let numberOfSimilarOrders = 0;
    let comparator = (a, b) => {

        // Convert each toppings array to a string and do a string comparison
        return a.toppings.toString().localeCompare(b.toppings.toString());

    };
    inputArray = inputArray.filter(validateAndSort);

    // Perform a QuickSort on the array.  We provide an anonymous function to do the comparisons.
    inputArray.sort(comparator);

    // Iterate through the array, with "order" being each item in the array.
    inputArray.map((order) => {
        let toppingsAsString = order.toppings.toString();
        if (order.toppings.length < 3) return;
        if (toppingsAsString === previousToppingsAsString) {
            numberOfSimilarOrders++;
        } else {
            if ((numberOfSimilarOrders === 1) && (previousToppingCount === 3) && (previousEmail) !== '') {
                results.push(previousEmail);
                log(previousEmail);
            }
            previousToppingsAsString = toppingsAsString;
            previousEmail = order.email;
            previousToppingCount = order.toppings.length;
            numberOfSimilarOrders = 1;
        }
    });
    return results.toString();
}
exports.printWinners1Modified = printWinners1Modified;


function printWinners2(inputArray, debug) {
    let log = logMethod(debug);
    var results = [];
    let hashTable = new Map();
    // Iterate through the array, with "order" being each item in the array.
    inputArray.map((order) => {
        if ((order.toppings.length === 3 && order.email !== '')) {
            let toppingsAsString = order.toppings.toString();
            let matchingValue = hashTable.get(toppingsAsString);
            if (matchingValue) {
                // This key was already in the hash table.
                matchingValue.duplicate = true;
            } else {
                // Insert a new object containing the email into the hash table, using the toppings as the key.
                hashTable.set(toppingsAsString, {
                    email: order.email,
                    duplicate: false
                });
            }
        }
    });
    // Iterate through the values in the hash table, with "value" being each value.
    hashTable.forEach((value) => {
        if (!value.duplicate) {
            log(value.email);
            results.push(value.email);
        }
    });
    return results.toString();
}
exports.printWinners2 = printWinners2;


function printWinners2Modified(inputArray, debug) {
    assert(Array.isArray(inputArray));
    let log = logMethod(debug);
    var results = [];
    let hashTable = new Map();
    inputArray = inputArray.filter(isValidOrder);
    // Iterate through the array, with "order" being each item in the array.
    inputArray.map((order) => {
        // if (!isValidOrder(order)) return;
        order.toppings.sort();
        let toppingsAsString = order.toppings.toString();
        let matchingValue = hashTable.get(toppingsAsString);
        if (matchingValue) {
            // This key was already in the hash table.
            matchingValue.duplicate = true;
        } else {
            // Insert a new object containing the email into the hash table, using the toppings as the key.
            hashTable.set(toppingsAsString, {
                email: order.email,
                duplicate: false
            });
        }
    });
    // Iterate through the values in the hash table, with "value" being each value.
    hashTable.forEach((value) => {
        if (!value.duplicate) {
            log(value.email);
            results.push(value.email);
        }
    });
    return results.toString();
}
exports.printWinners2Modified = printWinners2Modified;
