var Benchmark = require('benchmark');

var orders = require('./orders');
var methods = require('.');

var suite = new Benchmark.Suite;
var suite2 = new Benchmark.Suite;

suite
.add('printWinners1', function (){
  methods.printWinners1(orders.slice());
})
.add('printWinners2', function (){
  methods.printWinners2(orders.slice());
})
.on('cycle', function (e) {
  console.log(String(e.target))
})
.on('complete', function (){
  console.log(this.filter('fastest').map('name'));
})
.run({async:true});

suite2
.add('printWinners1Modified', function (){
  methods.printWinners1Modified(orders.slice());
})
.add('printWinners2Modified', function (){
  methods.printWinners2Modified(orders.slice());
})
.on('cycle', function (e) {
  console.log(String(e.target))
})
.on('complete', function (){
  console.log(this.filter('fastest').map('name'));
})
.run({async:true});
